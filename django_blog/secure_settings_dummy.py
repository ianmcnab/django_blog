# #########################################
# # SECURE SETTINGS Parameter File
# #########################################
# Used to contain all sensitve parameters which should not be visible in a source control system
# Parameters for each environment could alternately be sourced in from separate prod, test, dev files
# if it were neccessary to maintain password isolation between dev, test and prod teams.

# !!! IMPORTANT !!!
# This version is a DUMMY file as a reference for source control and is NOT USED.
# To use, rename this file to secure_settings.py and edit the values below appropriately

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Toggle active state here
# Valid values:
#   PROD  - Production
#   TEST  - Testing
#   DEVL  - Development
env_state = "PROD"


if env_state == "PROD":

    # No debug mode for Production
    DEBUG = False

    # Site Secret Key
    SECRET_KEY = '**************************************************'

    # Email configuration
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True
    EMAIL_HOST_USER = 'email_user'
    EMAIL_HOST_PASSWORD = 'email_pass'

    # Host configuration
    ALLOWED_HOSTS = ['example.com', '123.456.789.123', 'localhost']

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'db_name',
            'USER': 'db_user',
            'PASSWORD': 'db_password',
            'HOST': 'localhost',
            'PORT': '',
        }
    }


elif env_state == "TEST":

    # Allow debug in test mode
    DEBUG = True

    # Site Secret Key
    SECRET_KEY = '**************************************************'

    # Email configuration
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True
    EMAIL_HOST_USER = 'email_user'
    EMAIL_HOST_PASSWORD = 'email_pass'

    # Default to SQLite
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'testing_db.sqlite3'),
        }
    }


else:   # DEVL assumed

    # Allow debug in test mode
    DEBUG = True

    # Site Secret Key
    SECRET_KEY = '**************************************************'

    # Email configuration
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True
    EMAIL_HOST_USER = 'email_user'
    EMAIL_HOST_PASSWORD = 'email_pass'

    # Default to SQLite
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }
